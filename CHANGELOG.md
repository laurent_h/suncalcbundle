**2017-04-09**
- Ajout de la déclinaison du soleil pour le crépuscule : -5.83°, -6.3°, -7.083° et -8.5° ;
- Ajout de la déclinaison du soleil pour l'aube : -16.1°, -16.9°, -19.8° et -26° ; 
- Mise à jour de la class de tests ;

**2017-03-30**
- Version 1.1.0 - portage pour Symfony ;
- Création du bundle SuncalBundle Symfony ;
- Ajout des pages de tests dans le controlleur ZTestControlleur ;

**2017-03-29**
- Version 1.0.0 - version d'origine ;
- Fork du projet suncalc-php de gregseth disponible à l'adresse : https://github.com/gregseth/suncalc-php ;

