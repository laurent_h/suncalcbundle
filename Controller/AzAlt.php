<?php

namespace lilmodlelamed\SuncalcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AzAlt extends Controller {
    public $azimuth;
    public $altitude;

    function __construct($az, $alt) {
        $this->azimuth = $az;
        $this->altitude = $alt;
    }
}
