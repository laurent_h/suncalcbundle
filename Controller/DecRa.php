<?php

namespace lilmodlelamed\SuncalcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DecRa extends Controller
{    
    public $dec;
    public $ra;

    function __construct($d, $r) {
        $this->dec = $d;
        $this->ra  = $r;
    }
}

