<?php

namespace lilmodlelamed\SuncalcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DecRaDist extends DecRa {
    public $dist;

    function __construct($d, $r, $dist) {
        parent::__construct($d, $r);
        $this->dist = $dist;
    }
}