<?php

namespace lilmodlelamed\SuncalcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ZTestController extends Controller
{

   /**
    *@Route("/sunTimes", name="getSunTimes", options = { "utf8": true })
    */
    
     public function getSunTimes()
    {
     $latitude=48.8857293;
     $longitude=2.391339300000027;
     $heureLocale=\time();
     $dateLocale=new \DateTime();
     /* On appelle la fonction PHP */
     $phpDateSunInfo = date_sun_info($heureLocale, $latitude, $longitude);    

    
    /* On appelle la fonction sunCalc */ 
    $calc = new SunCalc($dateLocale, $latitude, $longitude);
    $sunTimes=$calc->getSunTimes();
    
    echo "<h1 style='color:#00445b;'>Affichage des heures de lever et de coucher du Soleil.</h1>";
    echo "<h2 style='color:#00445b;'>Coordonées (DD) : ". round($latitude,4) ."°, " . round($longitude,4) .'°, ' . $sunTimes['solarNoon']->format('e (T)');

    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Heures de lever du soleil, Zenith</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $sunTimes['solarNoon']->getTimestamp()) ."</td>"
           . "<td style='width:100px;'></td>" 
         . "</tr>"
       . "</table>";

    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Jour, 6° au dessus de l'horizon.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $sunTimes['goldenHourEnd']->getTimestamp()) ."</td>"
           . "<td style='width:100px;'></td>" 
         . "</tr>"
       . "</table>";

    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Fin du début du jour, 0.3° sous l'horizon.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $sunTimes['sunriseEnd']->getTimestamp()) ."</td>"
           . "<td style='width:150px;'></td>" 
         . "</tr>"
       . "</table>";

    $d1Stamp=$phpDateSunInfo['sunrise'];
    $d2Stamp=$sunTimes['sunrise']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Début du jour, 0.833° sous l'horizon.</caption>"
         . "<tr>"
           . "<th style='border: 1px solid black;'> data_sun_info </th>"
           . "<th style='border: 1px solid black;'> sunCalc </th>"
           . "<th style='border: 1px solid black;'> Ecart </th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";

    echo "<h1 style='color:#00445b;'>Trois moyennes étoiles dans la nuit.</h1>";
    echo "<h2 style='color:#00445b;'>Coordonées (DD) : ". round($latitude,4) ."°, " . round($longitude,4) .'°, ' . $sunTimes['solarNoon']->format('e (T)');

    echo "<br /><br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Crépuscule, -5.83° sous l'horizon</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;width:150px;'>" . date("Y-m-d H:i:s", $sunTimes['duskMediumStars1']->getTimestamp()) ."</td>"
           . "<td style='width:100px;'></td>" 
         . "</tr>"
       . "</table>";
    
    
    $d1Stamp=$phpDateSunInfo['civil_twilight_begin'];
    $d2Stamp=$sunTimes['dawn']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);
    
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Crépuscule civile, 6° sous l'horizon.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";
    
    $d1Stamp=$phpDateSunInfo['nautical_twilight_begin'];
    $d2Stamp=$sunTimes['nauticalDawn']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);

    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Crépuscule nautique, 12° sous l'horizon.</caption>"
         . "<tr>"
           . "<th style='border: 1px solid black;'> data_sun_info </th>"
           . "<th style='border: 1px solid black;'> sunCalc </th>"
           . "<th style='border: 1px solid black;'> Ecart </th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";
    
    $d1Stamp=$phpDateSunInfo['astronomical_twilight_begin'];
    $d2Stamp=$sunTimes['nightEnd']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Crépuscule astronomique, 18° sous l'horizon.</caption>"
         . "<tr>"
           . "<th style='border: 1px solid black;'> data_sun_info </th>"
           . "<th style='border: 1px solid black;'> sunCalc </th>"
           . "<th style='border: 1px solid black;'> Ecart </th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";
   
   
     echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Couché du soleil, fin du jour.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $sunTimes['goldenHour']->getTimestamp()) ."</td>"
           . "<td style='width:100px;'></td>" 
         . "</tr>"
       . "</table>";

     echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Début du crépuscule, 0.3° sous l'horizon.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $sunTimes['sunsetStart']->getTimestamp()) ."</td>"
           . "<td style='width:200px;'></td>" 
         . "</tr>"
       . "</table>";

    $d1Stamp=$phpDateSunInfo['sunset'];
    $d2Stamp=$sunTimes['sunset']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Couché du soleil, 0.833° sous l'horizon.</caption>"
         . "<tr>"
           . "<th style='border: 1px solid black;'> data_sun_info </th>"
           . "<th style='border: 1px solid black;'> sunCalc </th>"
           . "<th style='border: 1px solid black;'> Ecart </th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";
    
    $d1Stamp=$phpDateSunInfo['civil_twilight_end'];
    $d2Stamp=$sunTimes['dusk']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Crépuscule civil, 6° sous l'horizon.</caption>"
         . "<tr>"
           . "<th style='border: 1px solid black;'> data_sun_info </th>"
           . "<th style='border: 1px solid black;'> sunCalc </th>"
           . "<th style='border: 1px solid black;'> Ecart </th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";

    $d1Stamp=$phpDateSunInfo['nautical_twilight_end'];
    $d2Stamp=$sunTimes['nauticalDusk']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Crépuscule nautique, 12° sous l'horizon.</caption>"
         . "<tr>"
           . "<th style='border: 1px solid black;'> data_sun_info </th>"
           . "<th style='border: 1px solid black;'> sunCalc </th>"
           . "<th style='border: 1px solid black;'> Ecart </th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";

    $d1Stamp=$phpDateSunInfo['astronomical_twilight_end'];
    $d2Stamp=$sunTimes['night']->getTimestamp();
    $datetime1 = new \DateTime(date("Y-m-d H:i:s", $d1Stamp));
    $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
    $interval = $datetime1->diff($datetime2);
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Crépuscule astronomique, 18° sous l'horizon.</caption>"
         . "<tr>"
           . "<th style='border: 1px solid black;'> data_sun_info </th>"
           . "<th style='border: 1px solid black;'> sunCalc </th>"
           . "<th style='border: 1px solid black;'> Ecart </th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d1Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td style='border: 1px solid black;'>" . $interval->format('%R%H:%I:%S') ."</td>"
          . "</tr>"
       . "</table>";

     echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Nuit noir.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:150px; text-align:center;'>sunCalc</th>"
            . "<th style='width:100px;'></th>" 
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $sunTimes['nadir']->getTimestamp()) ."</td>"
           . "<td style='width:100px;'></td>" 
         . "</tr>"
       . "</table>";
    
    exit;
    }

    /**
    *@Route("/sunPosition", name="getSunPosition", options = { "utf8": true })
    */
    
     public function getSunPosition()
    {
     $latitude=48.8857293;
     $longitude=2.391339300000027;
     $dateLocale=new \DateTime();
          
    /* On appelle la fonction sunCalc */ 
    $calc = new SunCalc($dateLocale, $latitude, $longitude);
    $sunPosition=(array) $calc->getsunPosition($dateLocale);
    echo "<h1 style='color:#00445b;'>Calcul de la position du soleil.</h1>";
    echo "<h2 style='color:#00445b;'>Coordonées (DD) : ". round($latitude,4) ."°, " . round($longitude,4) .'°, ' . $dateLocale->format('e (T)');
    
    echo "<br /><br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Azimut et  Altitude.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:100px;'>Azimut</th>"
            . "<th style='border: 1px solid black; width:100px;'>Altitude</th>"
            . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black; width:150px; text-align:center;'>" . round($sunPosition['azimuth'],2) ." rad,<br />" . round(rad2deg($sunPosition['azimuth']),2) ."°</td>"
           . "<td style='border: 1px solid black; width:150px; text-align:center;'>" . round($sunPosition['altitude'],2) ." rad<br />" . round(rad2deg($sunPosition['altitude']),2) ."°</td>"
         . "</tr>"
       . "</table>";
    exit;
    }
    
   /**
    *@Route("/moonIllumination", name="getMoonIllumination", options = { "utf8": true })
    */
    
     public function getMoonIllumination()
    {
     $latitude=48.8857293;
     $longitude=2.391339300000027;
     $dateLocale=new \DateTime();
          
    /* On appelle la fonction sunCalc */ 
    $calc = new SunCalc($dateLocale, $latitude, $longitude);
    $moonIlluminiation=$calc->getMoonIllumination();
    
    echo "<h1 style='color:#00445b;'>Affichage de la phase d'illumination de la lune.</h1>";
    echo "<h2 style='color:#00445b;'>Coordonées (DD) : ". round($latitude,4) ."°, " . round($longitude,4) .'°, ' . $dateLocale->format('e (T)');
 
    $fraction=round($moonIlluminiation['fraction'],2);
    $phase=round($moonIlluminiation['phase'],2);
    $angle=round($moonIlluminiation['angle'],2);
    
    $typeLune="Transition";
    if ($fraction == 0) { $typeLune='Nouvelle lune';}
    if ($fraction == 1) { $typeLune='Pleine lune';}
    
    if ($phase <0.25) {$typePhase='Nouvelle lune';}
    if ($phase >= 0.25 && $phase <0.50) {$typePhase='Premier quartier';}
    if ($phase >=0.50 && $phase <0.75) {$typePhase='Pleine lune';}
    if ($phase >=0.75) {$typePhase='Dernier quartier';}
    
    echo "<br /><br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Fraction de lune (0 à 1) .</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:100px'>Fraction</th>"
            . "<th style='border: 1px solid black; width:100px'>Type</th>"
            . "<th style='width:60px'></th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black; text-align:center;'>". $fraction. "</td>"
           . "<td style='border: 1px solid black; text-align:center;'>" . $typeLune ."</td>"
           . "<td></td>" 
         . "</tr>"
       . "</table>";
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Phase de la lune (0 à 1).</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:100px'>Phase</th>"
            . "<th style='border: 1px solid black; width:100px'>Type</th>"
            . "<th style='width:60px'></th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black; text-align:center;'>". $phase. "°</td>"
           . "<td style='border: 1px solid black; text-align:center;'>" . $typePhase ."</td>"
           . "<td></td>" 
         . "</tr>"
       . "</table>";
    echo "<br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Angle médian en radian.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:100px'>Angle médian</th>"
            . "<th style='width:60px'></th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black; text-align:center;'>". $angle. "°</td>"
           . "<td></td>" 
         . "</tr>"
       . "</table>";
    
    exit;
    }
    
   /**
    *@Route("/moonTimes", name="getMoonTimes", options = { "utf8": true })
    */
    
     public function getMoonTimes()
    {
     $latitude=48.8857293;
     $longitude=2.391339300000027;
     $dateLocale=new \DateTime();
          
    /* On appelle la fonction sunCalc */ 
    $calc = new SunCalc($dateLocale, $latitude, $longitude);
    $moonTimes=$calc->getMoonTimes();
   
    echo "<h1 style='color:#00445b;'>Calcul de l'heure du lever et du coucher de la lune<h1>";
    echo "<h2 style='color:#00445b;'>Coordonées (DD) : ". round($latitude,4) ."°, " . round($longitude,4) .'°, ' . $dateLocale->format('e (T)');

    echo "<br /><br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Lever et coucher de la lune.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:100px;'>Lever</th>"
            . "<th style='border: 1px solid black; width:100px;'>Coucher</th>"
            . "<th style='width:100px;'></th>"
            . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black; width:150px; text-align:center;'>" . date("Y-m-d H:i:s", $moonTimes['moonrise']->getTimestamp()) ."</td>"
           . "<td style='border: 1px solid black; width:150px; text-align:center;'>" . date("Y-m-d H:i:s", $moonTimes['moonset']->getTimestamp()) ."</td>"
           . "<td></td>" 
         . "</tr>"
       . "</table>";
    exit;
    }    

   /**
    *@Route("/moonPosition", name="getMoonPosition", options = { "utf8": true })
    */
    
     public function getMoonPosition()
    {
     $latitude=48.8857293;
     $longitude=2.391339300000027;
     $dateLocale=new \DateTime();
          
    /* On appelle la fonction sunCalc */ 
    $calc = new SunCalc($dateLocale, $latitude, $longitude);
    $moonPosition=(array) $calc->getMoonPosition($dateLocale);
    
    echo "<h1 style='color:#00445b;'>Calcul de la position de la lune.</h1>";
    echo "<h2 style='color:#00445b;'>Coordonées (DD) : ". round($latitude,4) ."°, " . round($longitude,4) .'°, ' . $dateLocale->format('e (T)');
    
    echo "<br /><br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Distance, Azimut, Altitude.</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black; width:100px;'>Distance</th>"
            . "<th style='border: 1px solid black; width:100px;'>Azimut</th>"
            . "<th style='border: 1px solid black; width:100px;'>Altitude</th>"
            . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black; width:150px; text-align:center;'>" . number_format($moonPosition['dist'],5,","," ") ." km</td>"
           . "<td style='border: 1px solid black; width:150px; text-align:center;'>" . round($moonPosition['azimuth'],2) ." rad,<br />" . round(rad2deg($moonPosition['azimuth']),2) ."°</td>"
           . "<td style='border: 1px solid black; width:150px; text-align:center;'>" . round($moonPosition['altitude'],2) ." rad<br />" . round(rad2deg($moonPosition['altitude']),2) ."°</td>"
         . "</tr>"
       . "</table>";
    exit;
    }
    
    }   