# Introduction

Cette version est un fork de la version de GregSeth. 
Les sources de cette version sont disponibles sur https://github.com/gregseth/suncalc-php

SunCalc est une petite bibliothèque PHP pour calculer la position du soleil,
les phases du soleil (temps pour le lever du soleil, le coucher du soleil, le crépuscule, etc.),
la position de la lune et la phase lunaire pour l'emplacement et l'heure donnés,
basé sur la bibliothèque JavaScript créée par [Vladimir Agafonkin]
(http://agafonkin.com/en) ([@mourner](https://github.com/mourner)).

La plupart des calculs sont basés sur les formules fournies dans les excellents articles de réponses 
d'astronomie à propos de [la position du soleil] (http://aa.quae.nl/fr/reken/zonpositie.html)
et [des planètes] (http://aa.quae.nl/en/reken/hemelpositie.html).
Vous pouvez lire au sujet des différentes phases crépusculaires calculées par SunCalc
dans [l'article de Twilight sur Wikipedia] (http://en.wikipedia.org/wiki/Twilight).

## Prérequis :

## Fonctionnalités :

- Utilisation de la librairie de GregSeth dans une application Symfony ;
- Tests des fonctions depuis le controlleur ;

## Installation et configuration:

Installation depuis Bitbucker ou depuis [Composer](http://packagist.org :, 

```sh
composer require lilmodlelamed/suncalc-bundle
```

### ajout du bundle dans l'application

```php
// app/AppKernel.php
public function registerBundles()
{
    return array(
        // ...
        new lilmodlelamed\SuncalcBundle\lilmodlelamedSuncalcBundle(),
        // ...
    );
}
```

## Exemples de cas d'usages :

### Controller

```php
// initialisation de la class avec comme paramétre la date et l'heure du jour et les coordonnées géographiques pour Paris
$sc = new SunCalc(new DateTime(), 48.85, 2.35);

// Présentation des informations pour le levé du soleil depuis un objet de type [DateTime]
$sunTimes = $sc->getSunTimes();
$sunriseStr = $sunTimes['sunrise']->format('H:i');

// Pour obtenir la position du soleil (azimuth et altitude) au lever du soleil
$sunrisePos = $sc->getPosition($sunTimes['sunrise']);

// Pour obtenir l'azimuth du lever du soleil en degré
$sunriseAzimuth = $sunrisePos->azimuth * 180 / M_PI;
```

## Réferences

### Cacul de l'horaire du lever et du coucher du soleil

```php
SunCalc::getSunTimes()
```

Renvoie un tableau avec les index suivants (chacun est un objet de type `DateTime`) :

#### version 1.1.0
| Propriétés        | Description                                                                                       |
| ---------------   | ------------------------------------------------------------------------------------------------- |
| `sunrise`         | Le lever du soleil (le bord supérieur du soleil apparaît à l'horizon) : -0.833° sous l'horizon    |
| `sunriseEnd`      | Les extrémités du lever du soleil (le bord inférieur du soleil touche l'horizon) : -0.3°          |
| `goldenHourEnd`   | Heure matinale de fin du matin (lumière douce) : +6° au dessus de l'horizon                       |
| `solarNoon`       | Midi solaire (le soleil est dans la plus haute position)                                          |
| `goldenHour`      | Début de l'heure du coucher du soleil : +6° au dessus de l'horizon                                |
| `sunsetStart`     | Début du coucher du soleil (le bord inférieur du soleil touche l'horizon) : -0.3° sous l'horizon  |
| `sunset`          | Coucher du soleil (le soleil disparaît, le crépuscule civil du soir commence) : -0.833°           |
| `dusk`            | Crépuscule (début du crépuscule nautique du soir) : -6° sous l'horizon                            |
| `nauticalDusk`    | Crépuscule nautique (début du crépuscule astronomique du soir) : -12° sous l'horizon              |
| `night`           | La nuit commence (assez sombre pour les observations astronomiques) : -18° sous l'horizon         |
| `nadir`           | Nuit profonde (le moment le plus sombre de la nuit, le soleil est dans la position la plus basse) |
| `nightEnd`        | La nuit se termine (début du crépuscule astronomique du matin) : -18° sous l'horizon              |
| `nauticalDawn`    | L'aube nautique (début du crépuscule nautique du matin) : -12° sous l'horizon                     |
| `dawn`            | L'aube (fin du crépuscule nautique du matin, le crépuscule civil du matin commence) : -6°         |

#### version 1.2.0
| Propriétés        | Description                                                                                       |
| ----------------- | ------------------------------------------------------------------------------------------------- |
| `duskMediumStars1 | Crépuscule, 3 moyennes étoiles (Rabbi Berel Levin)       : -5.83° sous l'horizon                  |
| `duskMediumStars2 | Crépuscule, 3 moyennes étoiles (Rabbi Avrohom Altein)    : -6.3° sous l'horizon                   |
| `duskSmallStars1  | Crépuscule, 3 petites étoiles (Rabbi Berel Levin)        : -6.83° sous l'horizon                  |
| `duskSmallStars2  | Crépuscule, 3 petites étoiles (Rabbi David Zvi Hoffmann) : -7.083° sous l'horizon                 |
| `duskSmallStars3  | Crépuscule, 3 petites étoiles (Rabbi Y. M. Tukachinsky)  : -8.5° sous l'horizon                   |
| `aube1            | Aube, (Gra)                                              : -16.1° sous l'horizon                  |
| `aube2            | Aube, par défaut                                         : -16.9° sous l'horizon                  |
| `aube3            | Aube, Minhag Eretz Yisroel (Rabbi Y.M. Tukachinsky)      : -19.8° sous l'horizon                  |
| `aube4            | Aube, (Rav Chaim No’eh)                                   : -26° sous l'horizon                  |

### Position du soleil

```php
SunCalc::getSunPosition(/*DateTime*/ $timeAndDate)
```
Renvoie un objet avec les propriétés suivantes :

 * `altitude`: Altitude du soleil au-dessus de l'horizon en radians,
 i.e. `0` à l'horizon et` PI / 2` au zénith (au dessus de votre tête),
 * `azimuth`: Azimut du soleil en radians (direction le long de l'horizon, mesurée du sud vers l'ouest),
 i.e. `0` est au sud et `M_PI * 3/4` est au nord-ouest.

### Position de la lune

```php
SunCalc::getMoonPosition(/*DateTime*/ $timeAndDate)
```

Renvoie un objet avec les propriétés suivantes :

 * `altitude`: Altitude de la lune au-dessus de l'horizon en radians,
 * `azimuth`: Azimuth de lune en radians,
 * `dist`: Distance à la lune en kilomètres.


### Phase de la lune

```php
SunCalc :: getMoonIllumination()
```

Renvoie un tableau avec les propriétés suivantes :

 * `fraction`: Fraction lumineuse de la lune ; Varie de `0.0` (nouvelle lune) à` 1.0` (pleine lune)
 * `phase`: phase de lune; Varie de `0.0` à` 1.0`, décrit ci-dessous
 * `angle`: Angle médian en radians de la zone illuminé de la lune compté vers l'est à partir du point nord du disque ;
            La lune augmente si l'angle est négatif et diminue s'il est positif.

La valeur de la phase lune doit être interprétée comme suit :

| Phase | Nome            |
| -----:| --------------- |
| 0     | Nouvelle lune   |
|       | Lune croissance |
| 0.25  | Premier quartier|
|       | Lune gibbeuse   |
| 0.5   | Pleine lune     |
|       | lune gibbeuse   |
| 0.75  | Dernier quartier|
|       | Lune croissante |

### Progression de la lune et heures

```php
SunCalc::getMoonTimes($inUTC)
```

Renvoie un objet de type `DateTime` avec les index suivants :
 * `mmonrise`: Heure du lever de la lune ;
 * `moonset`: Heure de fixation de  la lune ;
 * `alwaysUp`: `True` si la lune ne s'élève jamais ou se couche et est toujours au-dessus de l'horizon pendant le jour ;
 * `alwaysDown`: `true` si la lune est toujours en dessous de l'horizon ;

Par défaut, on recherchera l'augmentation de la lune et par rapport au contexte local de l'utilisateur (de 0 à 24 heures).
Si `$ inUTC` est défini sur à 'true', on recherchera plutôt la date spécifiée de 0 à 24 heures UTC.

### Exemple d'utilisation depuis un contrôleur
## Class disponibles
* getMoonIllumination();
* getMoonPosition();
* getMoonTimes();
* getSunPosition();
* getSunTimes();

## Exemple
```php
// Acme\MainBundle\Controller\sunCalController.php

namespace lilmodlelamed\SuncalcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class sunCalcController extends Controller
{
   /**
    *@Route("/suntimes", name="getSunTimes", options = { "utf8": true })
    */
     public function getSunTimes()
    {
     $latitude=48.8857293;
     $longitude=2.391339300000027;
     $heureLocale=time();
     $dateLocale=new \DateTime();

     $calc = new SunCalc($dateLocale, $latitude, $longitude);
     $sunTimes=$calc->getSunTimes();

     $d2Stamp=$sunTimes['solarNoon']->getTimestamp();
     $datetime2 = new \DateTime(date ("Y-m-d H:i:s",$d2Stamp));
     echo "<br /><br />"
       . "<table style='color:#00445b; font-size:1rem;'>"
         . "<caption style='font-size:1.2rem;'>Heures de levé du soleil, Zenith</caption>"
         . "<tr>"
            . "<th style='border: 1px solid black;'> data_sun_info </th>"
            . "<th style='border: 1px solid black;'> sunCalc </th>"
            . "<th style='width:60px'></th>"
         . "</tr>"
         . "<tr>"
           . "<td style='border: 1px solid black;'></td>"
           . "<td style='border: 1px solid black;'>" . date("Y-m-d H:i:s", $d2Stamp) ."</td>"
           . "<td></td>" 
         . "</tr>"
       . "</table>";
}
```

# FIN